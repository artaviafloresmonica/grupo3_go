import javax.swing.ImageIcon;
import javax.swing.Icon;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;

public class Controlador implements MouseListener{
	private PanelTablero panelTablero;
	private TableroLogico tableroLogico;
	private Ficha ficha;
	private Mensajes mensaje;
	private VentanaTablero ventana;
	private ImageIcon fichaNegra, fichaBlanca;
	private int 	turno = 0;
	private int 	contadorB = 10;
	private int 	contadorN = 10;
	private int contadorComidasB = 0;
	private int contadorComidasN = 0;
	
	public Controlador(){
		tableroLogico = new TableroLogico();
		fichaBlanca = new ImageIcon("Img/FBlanca.png");
		fichaNegra = new ImageIcon("Img/FNegra.png");
	}
	
	public void setPanelTablero(PanelTablero panelTablero){
		this.panelTablero = panelTablero;
	}
	
	public void verificarComerFicha(int fila, int columna){
		if(columna<8){
			if(!tableroLogico.posicionVacia(fila, columna+1)){
				if(tableroLogico.getFicha(fila, columna).getColor()!=tableroLogico.getFicha(fila, columna+1).getColor()){
					if(tableroLogico.getLibertad(fila, columna+1)==0){
						tableroLogico.eliminarFicha(fila, columna+1);
							if(tableroLogico.getFicha(fila, columna).getColor()){ 
								contadorComidasB++;
								String fbC = Integer.toString(contadorComidasB);
								panelTablero.numFBComidas(fbC); 
							}
							else{
								contadorComidasN++;
								String fnC = Integer.toString(contadorComidasN);
								panelTablero.numFNComidas(fnC); 
							}
					}
				}
			}
		}
		if(columna>0){
			if(!tableroLogico.posicionVacia(fila, columna-1)){
				if(tableroLogico.getFicha(fila, columna).getColor()!=tableroLogico.getFicha(fila, columna-1).getColor()){
					if(tableroLogico.getLibertad(fila, columna-1)==0){
						tableroLogico.eliminarFicha(fila, columna-1);
						if(tableroLogico.getFicha(fila, columna).getColor()){ 
								contadorComidasB++;
								String fbC = Integer.toString(contadorComidasB);
								panelTablero.numFBComidas(fbC); 
						}else{
								contadorComidasN++;
								String fnC = Integer.toString(contadorComidasN);
								panelTablero.numFNComidas(fnC); 
						}
					}
				}
			}
		}
		if(fila<8){
			if(!tableroLogico.posicionVacia(fila+1, columna)){
				if(tableroLogico.getFicha(fila, columna).getColor()!=tableroLogico.getFicha(fila+1, columna).getColor()){
					if(tableroLogico.getLibertad(fila+1, columna)==0){
						tableroLogico.eliminarFicha(fila+1, columna);
						if(tableroLogico.getFicha(fila, columna).getColor()){ 
							contadorComidasB++;
							String fbC = Integer.toString(contadorComidasB);
							panelTablero.numFBComidas(fbC); 
						}else{
							contadorComidasN++;
							String fnC = Integer.toString(contadorComidasN);
							panelTablero.numFNComidas(fnC); 
						}
					}
				}
			}
		}
		if(fila>0){
			if(!tableroLogico.posicionVacia(fila-1, columna)){
				if(tableroLogico.getFicha(fila, columna).getColor()!=tableroLogico.getFicha(fila-1, columna).getColor()){
					if(tableroLogico.getLibertad(fila-1, columna)==0){
						tableroLogico.eliminarFicha(fila-1, columna);
						if(tableroLogico.getFicha(fila, columna).getColor()){ 
							contadorComidasB++;
							String fbC = Integer.toString(contadorComidasB);
							panelTablero.numFBComidas(fbC); 
						}else{
							contadorComidasN++;
							String fnC = Integer.toString(contadorComidasN);
							panelTablero.numFNComidas(fnC); 
						}
					}
				}
			}
		}
	}
	
	public void limpiarTablero(){
		for( int fila = 0;fila<tableroLogico.lengthFila();fila++){
			for(int columna = 0;columna<tableroLogico.lengthColumna();columna++){
				tableroLogico.eliminarFicha(fila,columna);
			}
		}
		contadorB=0;
		contadorComidasB=0;
		contadorComidasN=0;
		contadorN=0;
		String fb = Integer.toString(contadorB);
		panelTablero.numFB(fb); 
		String fbC = Integer.toString(contadorComidasB);
		panelTablero.numFBComidas(fbC); 
		String fn = Integer.toString(contadorN);
		panelTablero.numFN(fn); 
		String fnC = Integer.toString(contadorComidasN);
		panelTablero.numFNComidas(fnC); 
	}
		
	public void mouseClicked(MouseEvent click){
		if(turno == 0 && tableroLogico.posicionValida(click.getX(), click.getY())){ 
			for(int fila = 0;fila<tableroLogico.lengthFila();fila++){
				for(int columna = 0;columna<tableroLogico.lengthColumna();columna++){
					if(click.getX()>=tableroLogico.getX(columna) && click.getY()>=tableroLogico.getY(fila)){ 
						if(tableroLogico.rangoPosicion(fila, columna, click.getX(), click.getY())){ 
							if(tableroLogico.posicionVacia(fila, columna)){ 
								if(contadorN != 0){
									contadorN--;
									String fn = Integer.toString(contadorN);
									panelTablero.numFN(fn); 
									Ficha fichaN = new Ficha(false, click.getX(), click.getY(), fichaNegra);
									tableroLogico.setFicha(fichaN, fila, columna);
									verificarComerFicha(fila, columna);
									turno = 1;
								}	
								else{
								if(contadorComidasN>contadorComidasB){
									mensaje = new Mensajes("Ganador Negro");
									limpiarTablero();
								}
								else{
									if(contadorComidasB>contadorComidasN){
										mensaje = new Mensajes("Ganador Blanco");
										limpiarTablero();
										}
										else{
											if(contadorComidasB==contadorComidasN){
												mensaje = new Mensajes("Empate");
												limpiarTablero();
											}
										}		
									}
								}
							}
							else{
								mensaje = new Mensajes("Error Posicion");
							}		
						}	
					}
				}
			}
		}
		else{
			if(tableroLogico.posicionValida(click.getX(), click.getY())){
				for(int fila = 0;fila<tableroLogico.lengthFila();fila++){
					for(int columna = 0;columna<tableroLogico.lengthColumna();columna++){
						if(click.getX()>=tableroLogico.getX(columna) && click.getY()>=tableroLogico.getY(fila)){
							if(tableroLogico.rangoPosicion(fila, columna, click.getX(), click.getY())){
								if(tableroLogico.posicionVacia(fila, columna)){
									if(contadorB != 0){
										contadorB--;
										String fb = Integer.toString(contadorB);
										panelTablero.numFB(fb); 
										Ficha fichaB = new Ficha(true, click.getX(), click.getY(), fichaBlanca); 
										tableroLogico.setFicha(fichaB, fila, columna); 
										verificarComerFicha(fila, columna);
										turno = 0;
									}
									else{
										if(contadorComidasN>contadorComidasB){
											mensaje = new Mensajes("Ganador Negro");
											limpiarTablero();
										}
										else{
											if(contadorComidasB>contadorComidasN){
												mensaje = new Mensajes("Ganador Blanco");
												for( fila = 0;fila<tableroLogico.lengthFila();fila++){
													for( columna = 0;columna<tableroLogico.lengthColumna();columna++){
														limpiarTablero();
													}
												}
												contadorB=0;
												contadorComidasB=0;
												contadorComidasN=0;
												contadorN=0;
												String fb = Integer.toString(contadorB);
												panelTablero.numFB(fb); 
												String fbC = Integer.toString(contadorComidasB);
												panelTablero.numFBComidas(fbC); 
												String fn = Integer.toString(contadorN);
												panelTablero.numFN(fn); 
												String fnC = Integer.toString(contadorComidasN);
												panelTablero.numFNComidas(fnC); 
											}
											else{
												if(contadorComidasB==contadorComidasN){
													mensaje = new Mensajes("Empate");
													limpiarTablero();
												}
											}
										}
									}
								}
								else{
									mensaje = new Mensajes("Error Posicion");
								}	
							}
						}
					}
				}
			}
		}
		panelTablero.repaint();
	}
	
	public void mouseEntered(MouseEvent entrarFrame){

	}
	
	public void mousePressed(MouseEvent e){
		
	}
	
	public void mouseReleased(MouseEvent e){
		
	}
	
	public void mouseExited(MouseEvent salirFrame){
		System.out.println("Salio del frame");
	}
	
	public void actualizar(Graphics g){
		for(int fila = 0;fila<tableroLogico.lengthFila();fila++){
			for(int columna = 0;columna<tableroLogico.lengthColumna();columna++){
				if(tableroLogico.getFicha(fila, columna) != null){
					tableroLogico.getFicha(fila, columna).pintarFicha(g);
				}
				else{
					System.out.println("");
				}
			}
		}
	}
}//Fin de clase Controlador
