public class TableroLogico{
	private Ficha tableroLogico[][];
	private int posX[] = {155,222, 290, 359, 426, 495, 563, 630, 698}; 
	private int posY[] = {245, 312, 380, 449, 517, 584, 652, 720, 787};
	private boolean fichaQueCome;
	private Controlador controlador;
	boolean estado = false;

	public TableroLogico(){
		tableroLogico = new Ficha[9][9];
	}
	
	public void setFicha(Ficha ficha, int fila, int columna){
		ficha.setPosX(posX[columna]);
		ficha.setPosY(posY[fila]);
		tableroLogico[fila][columna] = ficha;
	}
	
	public Ficha getFicha(int fila, int columna){
		return tableroLogico[fila][columna];
	}
	
	public int getX(int columna){
		return posX[columna];
	}
	
	public int getY(int fila){
		return posY[fila];
	}
	
	public int lengthFila(){
		return tableroLogico.length;
	}
	
	public int lengthColumna(){
		return tableroLogico[0].length;
	}
		
	public boolean rangoPosicion(int fila, int columna, int x, int y){ 
		if (x >= posX[columna] && x <= posX[columna]+70 && y >= posY[fila] && y <= posY[fila]+70){ 
			return true;
		}
		return false;
	}
	
	public boolean posicionValida(int x, int y){
		for(int fila = 0;fila<posX.length;fila++){
			for(int columna = 0; columna<posX.length;columna++){
				if(rangoPosicion(fila, columna, x, y)){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean posicionVacia(int fila, int columna){
		if(tableroLogico[fila][columna] == null){
			return true;
		}
		return false;
	}
	
	public void eliminarFicha(int fila, int columna){
		tableroLogico[fila][columna]=null;
	}
	
	public int getLibertad(int fila, int columna){
		int totalLibertades = 0;
		if(columna<8){
				if(posicionVacia(fila, columna+1) || tableroLogico[fila][columna].getColor()  == tableroLogico[fila] [columna+1].getColor()){
						totalLibertades++;
				}
		}
				
		if(columna>0){
				if(posicionVacia(fila, columna-1) || tableroLogico[fila][columna].getColor()  == tableroLogico[fila] [columna-1].getColor()){
					totalLibertades++;
				}
		}
		
	if(fila<8){
		if(posicionVacia(fila+1, columna) || tableroLogico[fila][columna].getColor()  == tableroLogico[fila+1] [columna].getColor()){
			totalLibertades++;
		}
	}
	
	if(fila>0){
		if(posicionVacia(fila-1, columna) || tableroLogico[fila][columna].getColor()  == tableroLogico[fila-1] [columna].getColor()){
			totalLibertades++;
		}
	}
	return totalLibertades;
}
}//Fin de clase Tablero
