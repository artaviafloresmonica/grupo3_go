import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
public class VentanaInicio extends JFrame{
	private PanelInicio panelInicio;
	private VentanaTablero ventanaTablero;
	private ManejadorInicio manejadorInicio;
	
	public VentanaInicio(){
		super("Jugar");
		panelInicio = new PanelInicio(this);
		getContentPane().add(panelInicio);
		setSize(915, 940);
		setVisible(true);
		setLocationRelativeTo(null);
	}
}
