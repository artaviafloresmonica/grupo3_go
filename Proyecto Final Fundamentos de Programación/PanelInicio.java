import javax.swing.JPanel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JButton;
public class PanelInicio extends JPanel{
	private Icon panelInicio;
	private JButton btnJugar;
	private JButton btnIndicaciones;
	private JButton btnSalir;
	private VentanaInicio ventanaInicio;
	private ManejadorInicio manejadorInicio;
	
	public PanelInicio(VentanaInicio ventanaInicio){
		super();
		this.ventanaInicio=ventanaInicio;
		setLayout(null);
		manejadorInicio= new ManejadorInicio(ventanaInicio);
		panelInicio = new ImageIcon("Img/TableroJugar.jpg");
		btnJugar=new JButton(new ImageIcon("Img/botonjugar.png"));
		btnJugar.setActionCommand("Jugar");
		btnJugar.addActionListener(manejadorInicio);
		btnJugar.setBounds(350,590,217,63);
		add(btnJugar);
		
		btnIndicaciones=new JButton(new ImageIcon("Img/btnIndicacion.png"));
		btnIndicaciones.setActionCommand("Indicaciones");
		btnIndicaciones.addActionListener(manejadorInicio);
		btnIndicaciones.setBounds(780,850,100,33);
		add(btnIndicaciones);
		
		btnSalir=new JButton(new ImageIcon("Img/btnSalir.png"));
		btnSalir.setActionCommand("Salir");
		btnSalir.addActionListener(manejadorInicio);
		btnSalir.setBounds(15,850,100,33);
		add(btnSalir);;
	}
	 
	public void paint(Graphics g){
		super.paint(g);
		panelInicio.paintIcon(null, g, 0, 0);
	}
}
