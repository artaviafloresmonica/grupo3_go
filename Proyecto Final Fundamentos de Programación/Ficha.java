import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Graphics;
public class Ficha{
	private boolean color;
	private int posX, posY;
	private Icon imagen;
	
	public Ficha(boolean color, int posX, int posY, Icon imagen){
		this.posX=posX;
		this.posY=posY;
		this.color=color;
		this.imagen=imagen;
	}
	
	public void setPosX(int posX){
		this.posX=posX;
	}
	
	public int getPosX(){
		return posX;
	}
	
	public void setPosY(int posY){
		this.posY=posY;
	}
	
	public int getPosY(){
		return posY;
	}
	
	public void setColor(boolean color){
		this.color=color;
	}
	
	public boolean getColor(){
		return color;
	}
	
	public void setImagen(Icon imagen){
		this.imagen=imagen;
	}
	
	public Icon getImagen(){
		return imagen;
	}
	
	public void pintarFicha(Graphics g){
		imagen.paintIcon(null, g, posX, posY);
	}
}//Fin de clase
