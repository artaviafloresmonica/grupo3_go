import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;

public class VentanaTablero extends JFrame{
	private PanelTablero panelTablero;
	private Controlador controlador;
	
	public VentanaTablero(){
		super("Tablero");
		controlador = new Controlador();
		panelTablero = new PanelTablero(controlador);
		controlador.setPanelTablero(panelTablero);
		getContentPane().add(panelTablero);
		setSize(915, 940);
		setVisible(false);
		setLocationRelativeTo(null);
	}
}//Fin de clase VentanaTablero
