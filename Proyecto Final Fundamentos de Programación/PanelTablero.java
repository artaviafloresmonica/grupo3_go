import javax.swing.JPanel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class PanelTablero extends JPanel{
	private Icon panelTablero;
	private Controlador controlador;
	private JLabel fichaB;
	private JLabel fichaN;
	private JLabel fichaBComidas;
	private JLabel fichaNComidas;
	
	public PanelTablero(Controlador controlador){
		super();
		this.controlador = controlador;
		panelTablero = new ImageIcon("Img/TableroMod.png");
		setLayout(null);
		contFichas();
		contFichasComidas();
		addMouseListener(controlador);
	}
	
	public void contFichas(){
		Color colorFondo = new Color(197, 160, 139);
		fichaB = new JLabel();
		fichaB.setBounds(548, 170, 47, 28);
		fichaB.setText("10");
		fichaB.setForeground(Color.BLACK);
		fichaB.setHorizontalAlignment(SwingConstants.CENTER);
		fichaB.setBackground(colorFondo);
		fichaB.setOpaque(true);
		add(fichaB);
		
		fichaN = new JLabel();
		fichaN.setBounds(309, 170, 47, 28);
		fichaN.setText("10");
		fichaN.setForeground(Color.BLACK);
		fichaN.setHorizontalAlignment(SwingConstants.CENTER);
		fichaN.setBackground(colorFondo);
		fichaN.setOpaque(true);
		add(fichaN);
	}
	
	public void numFB(String numFB){
		fichaB.setText(numFB);
	}
	
	public void numFN(String numFN){
		fichaN.setText(numFN);
	}
		
	public void contFichasComidas(){
		Color colorFondo = new Color(216, 185, 165);
		fichaBComidas = new JLabel();
		fichaBComidas.setBounds(619, 170, 47, 28);
		fichaBComidas.setText("0");
		fichaBComidas.setForeground(Color.BLACK);
		fichaBComidas.setHorizontalAlignment(SwingConstants.CENTER);
		fichaBComidas.setBackground(colorFondo);
		fichaBComidas.setOpaque(true);
		add(fichaBComidas);
		
		fichaNComidas = new JLabel();
		fichaNComidas.setBounds(238, 170, 47, 28);
		fichaNComidas.setText("0");
		fichaNComidas.setForeground(Color.BLACK);
		fichaNComidas.setHorizontalAlignment(SwingConstants.CENTER);
		fichaNComidas.setBackground(colorFondo);
		fichaNComidas.setOpaque(true);
		add(fichaNComidas);
	}
	
	public void numFBComidas(String numFBComidas){
		fichaBComidas.setText(numFBComidas);
	}
	
	public void numFNComidas(String numFNComidas){
		fichaNComidas.setText(numFNComidas);
	}
	
	public void paint(Graphics g){
		super.paintComponent(g);
		panelTablero.paintIcon(null, g, 0, 0);
		fichaB .setFont(new Font("Montserrat",1,30));
		fichaN .setFont(new Font("Montserrat",1,30));
		fichaBComidas .setFont(new Font("Montserrat",1,30));
		fichaNComidas .setFont(new Font("Montserrat",1,30));
		controlador.actualizar(g);
	}
}//Fin de clase PanelTablero
