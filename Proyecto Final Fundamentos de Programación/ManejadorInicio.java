import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.ImageIcon;
import javax.swing.Icon;

public class ManejadorInicio implements ActionListener{
	private VentanaTablero ventanaTablero;
	private VentanaInicio ventanaInicio;
	private PanelInicio panelInicio;
	private Icon imagen;
	
	public ManejadorInicio(VentanaInicio ventanaInicio){
		this.ventanaInicio=ventanaInicio;
		ventanaTablero = new VentanaTablero();
	}
	
	public void setPanelInicio(PanelInicio panelInicio){
		this.panelInicio=panelInicio;
	}
	
	public void actionPerformed(ActionEvent evento){
		if(evento.getActionCommand().equals("Jugar")){
			ventanaInicio.setVisible(false);
			ventanaTablero.setVisible(true);
		}
		if(evento.getActionCommand().equals("Indicaciones")){
			imagen = new ImageIcon("Img/indicaciones.png");
			UIManager.put("OptionPane.background", new Color(44, 35, 42)); 
			UIManager.put("Panel.background", new Color(44, 35, 42)); 
			JOptionPane.showMessageDialog(null,imagen,"Indicaciones", JOptionPane.PLAIN_MESSAGE);
		}
		if(evento.getActionCommand().equals("Salir")){
			ventanaInicio.dispose();
		}
	}
}
