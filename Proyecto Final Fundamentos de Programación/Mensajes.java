import java.awt.Font;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.ImageIcon;
import javax.swing.Icon;

public class Mensajes{
	private Icon imagen;
	public Mensajes(String mensaje){
		if(mensaje.equalsIgnoreCase("Error Posicion")){
			imagen = new ImageIcon("Img/errorB.png");
			UIManager.put("OptionPane.background", new Color(32, 24, 37)); 
			UIManager.put("Panel.background", new Color(32, 24, 37)); 
			JOptionPane.showMessageDialog(null,imagen,"Posicion Invalida", JOptionPane.PLAIN_MESSAGE);	
		}
		if(mensaje.equalsIgnoreCase("Sin fichas")){
			imagen = new ImageIcon("Img/error2B.png");
			UIManager.put("OptionPane.background", new Color(32, 24, 37)); 
			UIManager.put("Panel.background", new Color(32, 24, 37)); 
			JOptionPane.showMessageDialog(null,imagen,"Fichas insuficientes", JOptionPane.PLAIN_MESSAGE);	
		}
		
		if(mensaje.equalsIgnoreCase("Ganador Negro")){
			imagen = new ImageIcon("Img/ganadornegro.png");
			UIManager.put("OptionPane.background", new Color(32, 24, 37)); 
			UIManager.put("Panel.background", new Color(32, 24, 37)); 
			JOptionPane.showMessageDialog(null,imagen,"El negro ha ganado", JOptionPane.PLAIN_MESSAGE);	
		}
		
		if(mensaje.equalsIgnoreCase("Ganador Blanco")){
			imagen = new ImageIcon("Img/ganadorblanco.png");
			UIManager.put("OptionPane.background", new Color(32, 24, 37)); 
			UIManager.put("Panel.background", new Color(32, 24, 37)); 
			JOptionPane.showMessageDialog(null,imagen,"El blanco ha ganado", JOptionPane.PLAIN_MESSAGE);	
		}
		
		if(mensaje.equalsIgnoreCase("Empate")){
			imagen = new ImageIcon("Img/empate.png");
			UIManager.put("OptionPane.background", new Color(32, 24, 37)); 
			UIManager.put("Panel.background", new Color(32, 24, 37)); 
			JOptionPane.showMessageDialog(null,imagen,"El juego se ha empatado", JOptionPane.PLAIN_MESSAGE);	
		}
	}
}
